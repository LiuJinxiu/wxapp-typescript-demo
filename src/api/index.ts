
import request from '../utils/request'

export function api(data: Object = {}) {
    return request({
        url: `/pubcloud/system/app/getMiniWebLogo`,
        method: 'get',
        data
    })
}