
const baseUrl = 'https://www.pubcloud.com'

export default function request(obj: any = {}): Promise<object> {
    return new Promise((resolve, reject) => {
        const { url, data, method, header, dataType } = obj;
        wx.request({
            url: baseUrl + url,
            data,
            method,
            header,
            dataType,
            success: (res: any) => {
                res.data.success ? resolve(res) : reject(res)
            },
            fail: (err: any) => {
                reject(err)
            }
        })
    })
}