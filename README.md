小程序 DEMO

- 使用 gulp 构建（支持 typescript 和 less）
- 使用 typescript 编译
- 使用 tslint + prettier 格式代码规范
- 使用小程序官方 typing 库

```bash
# 安装依赖
npm install

# 全局安装依赖
npm install gulp prettier typescript --global

# 安装 src 项目依赖
cd src
npm install
# 安装完成，需要在小程序开发工具里【工具】-【构建npm】

# 启动代码
npm run dev

# 打包代码
npm run build
```

## 项目结构

```
├─dist                              //编译之后的项目文件（带 sorcemap，支持生产环境告警定位）
├─src                               //开发目录
│  │  app.ts                        //小程序起始文件
│  │  app.json
│  │  app.less
│  │
│  ├─components                     //组件
│  ├─utils                           //工具库
│  ├─config                           //配置文档
│  ├─pages                          //小程序相关页面
│
│  project.config.json              //小程序配置文件
│  gulpfile.js                      //工具配置
│  package.json                     //项目配置
│  README.md                        //项目说明
│  tsconfig.json                     //typescript配置
│  tslint.json                     //代码风格配置
```

